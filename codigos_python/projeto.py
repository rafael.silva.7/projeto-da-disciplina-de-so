import psutil
import time
a=int(input())
if a==0:
    print("numero inválido")
    exit()
else:
    for i in range(0,a):
    # Obtém as informações sobre a memória virtual
        virtual_memory = psutil.virtual_memory()

    # Obtém o total de memória virtual disponível em bytes
        total_memory = virtual_memory.total

    # Obtém a quantidade de memória virtual usada em bytes
        used_memory = virtual_memory.used

    # Obtém a porcentagem de uso da memória virtual
        memory_percent = virtual_memory.percent

    # Obtém a quantidade de memória virtual livre em bytes
        free_memory = virtual_memory.available

    # Converte os valores para gigabytes com duas casas decimais
        total_memory_gb = round(total_memory / (1024**3), 2)
        used_memory_gb = round(used_memory / (1024**3), 2)
        free_memory_gb = round(free_memory / (1024**3), 2)

    # Imprime as informações obtidas
        print("Total de Memória Virtual: {} GB".format(total_memory_gb))
        print("Memória Virtual Usada: {} GB".format(used_memory_gb))
        print("Porcentagem de Uso da Memória Virtual:{}%".format(memory_percent))
        print("Memória Virtual Livre: {} GB".format(free_memory_gb))

    # Aguarda 1 segundo antes de atualizar as informações novamente
        time.sleep(2)
exit()
